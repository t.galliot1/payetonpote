<?php

namespace App\Controller;

use App\Entity\Campaign;
use App\Form\CampaignType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Participant;

/**
 * @Route("/campaign")
 */
class CampaignController extends AbstractController
{
    /**
     * @Route("/new", name="campaign_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $campaign = new Campaign();
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);
        $campaign->setName($request->request->get('name'));

        if (isset($_GET['campaign_name'])) {
            $campaign_name = $_GET['campaign_name'];
            
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $campaign->setId();
            $em->persist($campaign);
            $em->flush();

            $id = $campaign->getId();

            return $this->redirectToRoute('campaign_show', compact("id"));
        }
        
        if (isset($_GET['campaign_name'])) {
            return $this->render('campaign/new.html.twig', [
                'campaign' => $campaign,
                'form' => $form->createView(),
                'campaign_name' => $campaign_name,
            ]);
        }

        else {
            return $this->render('campaign/new.html.twig', [
                'campaign' => $campaign,
                'form' => $form->createView(),
            ]);
        }
    }

    /**
     * @Route("/{id}", name="campaign_show", methods="GET")
     */
    public function show(Campaign $campaign): Response
    {
        $query = 'SELECT * FROM participant
        LEFT JOIN payment ON payment.participant_id = participant.id
        WHERE campaign_id = "' . $campaign->getId() . '"';
        
        $statement =  $this->getDoctrine()
                           ->getManager()
                           ->getConnection()
                           ->prepare($query);
        
        $statement->execute();
        
        $participantWithParticipations = $statement->fetchAll();

        $total_amount = 0;
        $total_participant = count($participantWithParticipations);

        for ($i = 0; $i <= ($total_participant - 1); $i++) {
            $total_amount += $participantWithParticipations[$i]['amount'];
        }

        $total_amount2 = $total_amount / 100;
        $objectif = round($total_amount2 / $campaign->getGoal() * 100);

        return $this->render('campaign/show.html.twig', compact('campaign', 'participantWithParticipations', 'total_participant', 'total_amount2', 'objectif'));
    }

    /**
     * @Route("/{id}/payment", name="campaign_payment", methods="GET")
     */
    public function payment(Campaign $campaign): Response
    {
        $amount_participant = $_GET["amount_participant"] ?? "";
        return $this->render("campaign/payment.html.twig", compact("campaign", "amount_participant"));
    }

    /**
     * @Route("/{id}/edit", name="campaign_edit", methods="GET|POST")
     */
    public function edit(Request $request, Campaign $campaign): Response
    {
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('campaign_edit', ['id' => $campaign->getId()]);
        }

        return $this->render('campaign/edit.html.twig', [
            'campaign' => $campaign,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="campaign_delete", methods="DELETE")
     */
    public function delete(Request $request, Campaign $campaign): Response
    {
        if ($this->isCsrfTokenValid('delete'.$campaign->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($campaign);
            $em->flush();
        }

        return $this->redirectToRoute('campaign_index');
    }

    /**
     * @Route("/{id}/spending", name="campaign_spending", methods="GET")
     */
    public function spending(Campaign $campaign): Response
    {
        $amount_participant = $_GET["amount_participant"];
        return $this->render("campaign/dépense.html.twig", compact("campaign", "amount_participant"));
    }
}
