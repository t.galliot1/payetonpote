<?php

namespace App\Controller;

use App\Entity\Spending;
use Stripe\Stripe;
use Stripe\Charge;
use App\Entity\Payment;
use App\Entity\Participant;
use App\Entity\Campaign;
use App\Form\PaymentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/spending")
 */
class SpendingController extends AbstractController
{
    
    /**
     * @Route("/depense", name="spending_charge", methods="GET|POST")
     */
    public function charge(Request $request): Response
    {
        $campaign_id = $request->request->get('campaign_id');

        $campaign = $this->getDoctrine()->getRepository(Campaign::class)->find($campaign_id);

        //Enregistrer le participant
        $participant = new Participant();

        $participant->setName($request->request->get("name"));
        $participant->setEmail($request->request->get("email"));
        $participant->setCampaign($campaign);

        $em = $this->getDoctrine()->getManager();
        $em->persist($participant);
        $em->flush();

        //Enregistrer le payment qui est dépendant du participant
        $spending = new Spending();
        $spending->setParticipant($participant);
        $spending->setAmount($request->request->get('amount'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($spending);
        $em->flush();

        return $this->redirectToRoute('campaign_show', [
            'id' => $campaign_id
        ]);

        //Redirection vers la fiche campagne
    }
}
